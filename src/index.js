import fs from 'node:fs';
import os from 'node:os';
import path from 'node:path';
import tar from 'tar';
import { nanoid } from 'nanoid';
import ignore from 'ignore';
import fnetListFiles from '@fnet/list-files';

/**
 * Creates a gzipped tarball of files in the source directory that match the provided glob patterns, 
 * excluding the patterns specified in .gitignore file if present.
 * Maintains the directory structure as in the source directory and supports recursion.
 * Ensures that each file or directory is only added once even if matched by multiple patterns.
 * @param {Object} args - The object containing function parameters.
 * @param {string} args.sourceDir - The path of the source directory where the files will be included in the tarball.
 * @param {string[]} args.pattern - The list of glob patterns to match in the source directory.
 * @param {string} [args.outputDir] - The directory where the generated tarball file will be saved. 
 *                                    If not specified, the operating system's temporary directory is used.
 * @param {boolean} [args.useGitIgnore=false] - Whether to use .gitignore file to exclude files from being included in the tarball.
 * @returns {Promise<Object>} Returns an object containing the full path of the generated tarball file, 
 *                            and lists of the added files and folders.
 */
export default async ({ sourceDir, pattern, outputDir, useGitIgnore = false }) => {
  const tempDir = outputDir || os.tmpdir();
  const tarballFileName = `${nanoid()}.tgz`;
  const tarballFilePath = path.join(tempDir, tarballFileName);
  pattern = Array.isArray(pattern) ? pattern : pattern ? [pattern] : [];

  const ig = ignore();
  if (useGitIgnore) {
    try {
      const gitignore = await fs.promises.readFile(path.join(sourceDir, '.gitignore'), 'utf8');
      ig.add(gitignore.split(/\r?\n/));
    } catch (error) {
      console.error('Error reading .gitignore file:', error);
    }
  }

  const filesList = [];
  const foldersList = [];

  const files = await fnetListFiles({ pattern, dir: sourceDir, nodir: false, absolute: true });
  for (const file of files) {
    const filePath = file;
    const relativePath = path.relative(sourceDir, filePath);
    if (ig.ignores(relativePath)) {
      continue;
    }
    const stats = await fs.promises.lstat(filePath);
    if (stats.isDirectory()) {
      foldersList.push(relativePath);
    } else {
      filesList.push(relativePath);
    }
  }

  await tar.c(
    {
      gzip: true,
      file: tarballFilePath,
      cwd: sourceDir
    },
    [...filesList, ...foldersList]
  );

  return {
    path: tarballFilePath,
    files: filesList,
    folders: foldersList
  };
};