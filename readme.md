# @fnet/dir-gzipper

This code represents a software package or module named `@fnet/dir-gzipper`. Its function is to create a gzipped tarball of files in a given source directory. The files included are determined by glob patterns provided as parameters. If a `.gitignore` file is present, the package can also exclude files based on the patterns specified in that file. This is set to true by default but can be toggled off.

### Main Functionality

The code reads files from a source directory and uses *glob patterns* to identify specific files to be included in the gzipped tarball. These glob patterns can be specified by the end user. Glob patterns are patterns using wildcards, and are a way to identify multiple files without needing to specify each one explicitly. For instance, the glob pattern `*.txt` would match all text files.

The code also reads a `.gitignore` file if present and excludes any files that match the patterns in that file - mimicking git's own behavior. This feature can be deactivated.

The gzipped tarball created by the module maintains the directory structure from the source directory and supports recursion, which means that it can include files from subdirectories within the source directory as well.

The tarball will be saved in a location dependent on whether the user has defined an output directory or not. If an output directory is defined, the tarball will be saved to that directory, otherwise, it will be saved to the operating system's temporary directory.

### Edge Case Handling

The code also includes functionality to tackle potential edge cases:

- It ensures that even if files or directories are matched by several glob patterns, they are included in the tarball only once.
- If no glob patterns are provided, the code will take an empty array `[]` as the default.

### Return Value

Finally, the returned value of the code is a JavaScript Promise that resolves with an object. This object contains the full path of the created tarball file, and lists of the files and folders that have been included in the tarball.
